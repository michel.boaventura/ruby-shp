require 'lib/Poly.rb'
require 'rubygems'
require 'spec'

class Test_Poly

  describe Poly do

    before do
      size_of_image = [800,600]
      box = [0,0,100,100]
      @numparts = 1
      @numpoints = 3
      @parts = [0]
      @points = [0,0,50,50,200,45]
      @poly = Poly.new(5,box,@numparts,@numpoints,@parts,@points,1)
    end

    it "should have a right polygon_type" do
      @poly.polygon_type.should == 5
    end
    it "should have the right numparts" do
      @poly.numparts.should == @poly.parts.size
    end
    it "should give the points" do
      @poly.get_all.size.should == @poly.parts.size
    end
  end
end
