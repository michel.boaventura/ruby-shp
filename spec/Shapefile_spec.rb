require 'lib/Shapefile.rb'
require 'lib/Poly.rb'
require 'spec'

class Test_Shapefile

  describe Shapefile do
    before do
      @shapefile = Shapefile.new 'spec/test.shp'
    end

    it "should have file code = 9994" do
      @shapefile.file_code.should == 9994
    end

    it "should have a file length > 0" do
      @shapefile.file_length.should > 0
    end

    it "should have a version equal 1000" do
      @shapefile.version == 1000
    end

    it "should implement to_s" do
      @shapefile.to_s.should == true
    end
    it "should give a valid shape type" do
      [0,1,3,5,8,11,13,15,18,21,23,25,28,31].
        include?(@shapefile.shape_type).should == true
    end
    it "should get the records" do
      @shapefile.shapes.nil?.should == false
    end
    it "should have valid x range" do
      (@shapefile.xmax - @shapefile.xmin).should > 0
    end
    it "should have valid y range" do
      (@shapefile.ymax - @shapefile.ymin).should > 0
    end
    it "should try to find a polygon which contains a point" do
      @shapefile.find([300,300]).should == nil
    end
  end
end
