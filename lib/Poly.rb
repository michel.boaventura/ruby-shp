require 'rubygems'

class Poly

  attr_reader :polygon_type,:points,:box,:numpoints,:numparts,:parts,:my_id

  def initialize(polygon_type,box,numparts,numpoints,parts,points,my_id)
    el = 0
    @my_id = my_id
    @polygon_type = polygon_type
    @box = box
    @points = []
    @numparts = numparts
    @numpoints = numpoints
    @parts = parts

    (0..(@numparts-1)).each do |el|
      @points[el] = points[@parts[el],2*((@parts[el+1] || @numpoints) - @parts[el])]
    end
  end

  def get_all
    ret = []
    @points.each { |el| ret << Poly.new(@polygon_type,@box,1,el.size/2,[0],el,@my_id) }
    ret
  end
end
