class Helpers
  public
  def self.is_left(p0, p1, p2)
    (p1[0] - p0[0]) * (p2[1] - p0[1]) - (p2[0] - p0[0]) * (p1[1] - p0[1])
  end

  def self.is_inside(p,v)
    wn = 0
    0.upto(v.size / 2 - 2) do |i|
      vi1  = v[2*i + 1]
      vi11 = v[2*i + 3]
      if (vi1 <= p[1]) then
        if (vi11 > p[1]) then
          if is_left([v[2*i],v[2*i + 1]], [v[2*i + 2],v[2*i + 3]], p) > 0 then
            wn += 1
          end
        end
      else
        if (vi11 <= p[1]) then
          if is_left([v[2*i],v[2*i + 1]], [v[2*i + 2],v[2*i + 3]], p) < 0 then
            wn -= 1
          end
        end
      end
    end
    return wn != 0
  end

  def self.put_on_scale(xmin,ymin,xmax,ymax,size_of_image,points)
    points_on_scale = []
    rotate = true
    dx = xmax - xmin
    dy = ymax - ymin
    kx = size_of_image[0] / dx
    ky = size_of_image[1] / dy

    points.each do |el|
      tmp = rotate ? (el - xmin) * kx : (el - ymin) * ky
      rotate = !rotate
      points_on_scale << tmp
    end
    points_on_scale
  end

  def self.put_out_of_scale(xmin,ymin,xmax,ymax,size_of_image,points)
    dx = xmax - xmin
    dy = ymax - ymin
    kx = (dx * points[0] / size_of_image[0]) + xmin
    ky = (dy * points[1] / size_of_image[1]) + ymin
    [kx,ky]
  end

  def self.image(shape,image,ano,ranges)
    colors = %w( #CCFFFF #99FF99 #FFFF66 #FFCC33 #FF0000 )
    shape.shapes.each do |el|
      my_color = Homicidio.color(el.my_id,ano,ranges,colors)
      image.insert_poly(el,my_color)
    end
    image.to_s
  end
end
