require 'Helpers.rb'
class Shapefile
  public

  attr_reader :file_code, :file_length
  attr_reader :version, :shape_type
  attr_reader :xmin, :xmax, :ymin, :ymax
  attr_reader :zmin, :zmax, :mmin, :mmax
  attr_reader :shp, :shapes, :box
  attr_reader :header,:header_array

  def to_s
    p "File Code = #{@file_code}"
    p "File Length = #{@file_length}"
    p "File Version = #{@version}"
    p "Shape Type = #{@shape_type}"
    p "Xmin = #{@xmin}"
    p "Xmax = #{@xmax}"
    p "Ymin = #{@ymin}"
    p "Ymax = #{@ymax}"
    p "Zmin = #{@zmin}"
    p "Zmax = #{@zmax}"
    p "Mmin = #{@mmin}"
    p "Mmax = #{@mmax}"
    true
  end

  def initialize(filename)
    @shp = File.open(filename,'r')
    load_header
    get_records
  end

  def find(point)
    @shapes.each do |el|
      el.points.each do |e|
        return el.my_id if Helpers.is_inside(point,e)
      end
    end
    return nil
  end

  private

  def load_header
    @file_code,un1,un2,un3,un4,un5,
    @file_length,@version,@shape_type,
    @xmin,@ymin,@xmax,@ymax,@zmin,
    @zmax,@mmin,@mmax =
    @shp.read(100).unpack('N'*7+'V'*2+'E'*8)
    @box = [@xmin,@ymin,@xmax,@ymax]
  end

  def load_record_header
    @shp.read(8).unpack('N'*2)
  end

  def load_record_type
    @shp.read(4).unpack('V').first
  end

  def load_record_poly(my_id)
    box       = @shp.read(8*4).unpack('E'*4)
    numparts  = @shp.read(4).unpack('V').first
    numpoints = @shp.read(4).unpack('V').first * 2
    parts     = @shp.read(4*numparts).unpack('V'*numparts)
    points    = @shp.read(8*numpoints).unpack('E'*numpoints)
    [box,numparts,numpoints,parts,points,my_id]
  end

  def get_records
    @shapes = []
    while !@shp.eof?
      my_id,size = load_record_header
      type = load_record_type
      Poly.new(type,*load_record_poly(my_id)).get_all.each do |el|
        @shapes << el
      end
    end
    return true
  end
 end
